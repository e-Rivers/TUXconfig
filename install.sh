#!/bin/sh

#   vim:fileencoding=utf-8:foldmethod=marker
#
#    ██                    ██              ██  ██
#   ░██                   ░██             ░██ ░██
#   ░██ ███████   ██████ ██████  ██████   ░██ ░██
#   ░██░░██░░░██ ██░░░░ ░░░██░  ░░░░░░██  ░██ ░██
#   ░██ ░██  ░██░░█████   ░██    ███████  ░██ ░██
#   ░██ ░██  ░██ ░░░░░██  ░██   ██░░░░██  ░██ ░██
#   ░██ ███  ░██ ██████   ░░██ ░░████████ ███ ███
#   ░░ ░░░   ░░ ░░░░░░     ░░   ░░░░░░░░ ░░░ ░░░ 
#     ████████                ██           ██  
#    ██░░░░░░                ░░  ██████   ░██  
#   ░██         █████  ██████ ██░██░░░██ ██████
#   ░█████████ ██░░░██░░██░░█░██░██  ░██░░░██░ 
#   ░░░░░░░░██░██  ░░  ░██ ░ ░██░██████   ░██  
#          ░██░██   ██ ░██   ░██░██░░░    ░██  
#    ████████ ░░█████ ░███   ░██░██       ░░██ 
#   ░░░░░░░░   ░░░░░  ░░░    ░░ ░░         ░░
#
#    Author: Emilio Rivers (e-Rivers)
#

#  #####  STEP 1  ##### {{{
# This is just to validate that the system has EFI and to stablish wireless connection
echo "$1"
if [ "$1" = "1" ]; then
    # Verify UEFI mode
    efi_content=$(ls /sys/firmware/efi/efivars | wc -l || 0)
    if [ "$efi_content" -gt 0 ]; then

        echo "You have a UEFI system."
        
        # Instructions to connect to Internet
        echo -e "\n\nIf you wanna connect to a wireless network follow the next steps:"
        echo -e "● Type iwctl."
        echo -e "● One iwd prompt has opened, check your wireless device name with \e[3mdevice list\e[0m."
        echo -e "● Scan for networks with \e[3mstation <<device>> scan\e[0m."
        echo -e "● To actually see the available networks with \e[3mstation <<device>> get-networks\e[0m."
        echo -e "● Finally, connect to the network via \e[3mstation <<device>> connect <<SSID>>\e[0m."

    else
        echo -e "\e[1mYou DON'T have a UEFI system.\e[0m Since this installation file only works with that, you can't proceed unless you enable it."
    fi
#  ##### }}}

#  #####  STEP 2  ##### {{{
# This is to configure everything prior hard disk configuration
elif [ "$1" = "2" ]; then
    # This is done to change clock and timezone configuration
    loadkeys la-latin1
    timedatectl set-ntp true
    timedatectl set-timezone America/Mexico_City
    
    # Instructions for disk partitioning
    echo -e "\n\nIt's time to partition the disk, create partitions in the following order:"
    echo -e "● Using any tool you want (cfdisk, fdisk, etc) format the disk to GPT."
    echo -e "● Create a partition of about 1GB for the BOOT partition (EFI partition type)."
    echo -e "● Create another partition for encrypted LVM using the remaining disk space (LVM partition type)."
    echo -e "● Save the changes, those are the only partitions needed since we're going to use logical volumes."
#  ##### }}}

#  #####  STEP 3  ##### {{{
# This step is to format the partitions and create the LVM setup
elif [ "$1" = "3" ]; then
    diskPartitions=( $(fdisk -l | grep ^/dev | awk '{ print $1 }') )

    # ### PARTITION FORMATTING
    # Format the EFI partition
    mkfs.fat -F32 ${diskPartitions[0]}
    # Encrypt the LVM partition
    cryptsetup luksFormat ${diskPartitions[1]}
    # Unlock the encrypted partition to configure LVM
    cryptsetup open --type luks ${diskPartitions[1]} cryptodisk

    # ### LVM FORMATTING
    # Configuration of the LVM partition
    pvcreate /dev/mapper/cryptodisk 
    vgcreate vg0 /dev/mapper/cryptodisk 

    echo -e "\n\nPartitions have been formatted, now it's time to configure the logical volumes, you can do this in two ways:"
    echo -e "\e[1mAuto:\e[0m"
    echo -e "● It will create a swap logical volume and a home logical volume only."
    echo -e "● Just answer the questions as requested to manage logical volumes sizes."
    echo -e "\e[1mManual:\e[0m"
    echo -e "● Configure by typing the commands on your own. (the volume group is vg0)"
    read -p "Choose an option [a/m]: " userSel

    if [ "$userSel" = "a" ]; then
        echo -e "\nEnter the sizes in the format \e[3m<size><uppercase units>\e[0m (e.g. 10GB) or type \e[3mREM\e[0m to use the remaining free space."
        read -p "Size of your swap partition: " swapSel
        lvcreate -L $swapSel vg0 -n lv_swap
        read -p "Size of your root partition: " rootSel
        lvcreate -L $rootSel vg0 -n arch_root
        read -p "Size of your home partition: " homeSel
        if [ "$homeSel" = "REM" ]; then
            lvcreate -l 100%FREE vg0 -n arch_home
        else
            lvcreate -L $homeSel vg0 -n arch_home
        fi

        # Activation of the volume group
        modprobe dm_mod
        vgscan
        vgchange -ay

        # Formatting of the logical volumes
        mkswap /dev/vg0/lv_swap
        mkfs.ext4 /dev/vg0/arch_root
        mkfs.ext4 /dev/vg0/arch_home

        # Mounting and basic system configuration
        mount /dev/vg0/arch_root /mnt
        mkdir /mnt/home
        mount /dev/vg0/arch_home /mnt/home
        mkdir /mnt/boot
        mount ${diskPartitions[0]} /mnt/boot
        swapon /dev/vg0/lv_swap

        # Install the kernel and basic necessary packages
        pacstrap /mnt base linux-zen linux-zen-headers linux-firmware
        # Filesystem table setup
        genfstab -U /mnt >> /mnt/etc/fstab
        # Get the custom config files
        git clone https://gitlab.com/e-Rivers/TUXconfig /mnt/etc/TUXconfig
        # Enter the Arch system for configuration
        arch-chroot /mnt

    elif [ "$userSel" = "m" ]; then
        echo "After configuring manually proceed with step 4."
    fi
#  ##### }}}

#  #####  STEP 4  ##### {{{
# This step is to install and configure Arch Linux on the previously formatted disk
elif [ "$1" = "4" ]; then
    diskPartitions=( $(fdisk -l | grep ^/dev | awk '{ print $1 }') )

    # Configure location, time and language
    ln -sf /usr/share/zoneinfo/America/Mexico_City /etc/localtime
    hwclock --systohc
    # Install base needed packages
    pacman -S neovim git lvm2 sudo grub efibootmgr dosfstools os-prober mtools zsh xorg xorg-xinit lightdm lightdm-webkit2-greeter networkmanager wpa_supplicant wireless_tools kitty tree plocate base-devel ncdu figlet man cups system-config-printer gutenprint xorg-xrandr xf86-video-fbdev xf86-video-vesa xf86-video-amdgpu xf86-video-ati xf86-video-intel xf86-video-nouveau nvidia picom wget zsh-autosuggestions zsh-syntax-highlighting zsh-history-substring-search exa neofetch pavucontrol ranger dialog bluez bluez-utils pulseaudio-bluetooth xdg-utils xdg-user-dirs pulseaudio brightnessctl checkbashisms rofi rofi-calc light-locker thunar gthumb virtualbox gimp iw crda wireless-regdb flameshot python-pillow guvcview xpad libreoffice mpv blueman redshift upower bat lxappearance fortune-mod inter-font celestia baobab stellarium
    # Locale configuration
    sed -i -r "s/^#en_US\.UTF-8 UTF-8/en_US\.UTF-8 UTF-8/" /etc/locale.gen
    locale-gen
    echo "LANG=en_US.UTF-8" >> /etc/locale.conf
    echo "KEYMAP=la-latin1" >> /etc/vconsole.conf
    # Hostname and hosts configuration
    read -p "What's gonna be your HOSTNAME? " userHost
    echo $userHost >> /etc/hostname
    echo "127.0.0.1     localhost" >> /etc/hosts
    echo "::1           localhost" >> /etc/hosts
    echo "127.0.1.1     ${userHost}.localdomain     ${userHost}" >> /etc/hosts
    # Users configuration
    read -p "Do you want to set a password for the root user? [y/n]: " rootPwd
    if [ "$rootPwd" = "y" ]; then
        passwd
    fi
    read -p "Write the username of your new user: " userName
    read -p "Write your real name for the user: " userReal
    useradd -m -c "$userReal" -s /bin/zsh -G lp,sys,audio,video,wheel,input $userName
    passwd $userName
    # Modification to config file to support encryption and LVM
    sed -i -r "s/^MODULES.*/MODULES=(intel_agp i915 amdgpu radeon nouveau)/" /etc/mkinitcpio.conf
    sed -i -r "s/^HOOKS.*/HOOKS=(base udev autodetect keyboard keymap modconf block encrypt lvm2 filesystems fsck)/" /etc/mkinitcpio.conf
    mkinitcpio -p linux-zen
    # Bootloader configuration
    grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=grub_uefi --recheck
    echo -e "\nGRUB_DISABLE_OS_PROBER=false" >> /etc/default/grub
    sed -i -r "s@^GRUB_CMDLINE_LINUX=.*@GRUB_CMDLINE_LINUX=\"cryptdevice=${diskPartitions[1]}:luks:allow-discards\"@" /etc/default/grub

    echo -e "WRITE (TWO TIMES) THE PASSWORD FOR THE GRUB BOOTLOADER:\n"
    GRUB_HASH=$(grub-mkpasswd-pbkdf2 | awk 'FNR == 3 {print $7}')
    echo "" >> /etc/grub.d/00_header
    echo "cat << EOF" >> /etc/grub.d/00_header
    echo "set superusers=\"R1V3R5-6RU8\"" >> /etc/grub.d/00_header
    echo "password_pbkdf2 R1V3R5-6RU8 ${GRUB_HASH}" >> /etc/grub.d/00_header
    echo "EOF" >> /etc/grub.d/00_header

    grub-mkconfig -o /boot/grub/grub.cfg
#  ##### }}}

#  #####  STEP 5  ##### {{{
# This step is to configure some tweaks and install desired packages
elif [ "$1" = "5" ]; then
    # Configuration based on TUXconfig files
    # Display Manager configuration 
    sed -i -r "s/#greeter-session=.*/greeter-session=lightdm-webkit2-greeter/" /etc/lightdm/lightdm.conf
    sed -i -r "s/#user-session=.*/user-session=awesome/" /etc/lightdm/lightdm.conf
    # Bootloader configuration
    sed -i -r "s/GRUB_TIMEOUT=.*/GRUB_TIMEOUT=-1/" /etc/default/grub
    cp -rf /etc/TUXconfig/GRUB/61_custom_leave_options /etc/grub.d/61_custom_leave_options
    /etc/TUXconfig/GRUB/setGrub.sh --set RiversWorkstation
    # Security configuration
    sed -i "s/# deny = 3/deny = 0/" /etc/security/faillock.conf
    # Pacman configuration
    sed -i -r "s/^#Color/Color\nILoveCandy/" /etc/pacman.conf
    sed -i -r "s/^#ParallelDownloads.*/ParallelDownloads = 5/" /etc/pacman.conf
    sed -i -r "/#\[multilib\]/{n;d}" /etc/pacman.conf
    sed -i -r "s@#\[multilib\]@\[multilib\]\nInclude = /etc/pacman.d/mirrorlist@" /etc/pacman.conf
    pacman -Syy
    # Sudo configuration
    echo "Defaults  passprompt=\"░▒▓[🔒] This is a free system, it's not password free system: \"" >> /etc/sudoers
    echo "Defaults  insults" >> /etc/sudoers
    echo "Defaults  timestamp_timeout=0" >> /etc/sudoers
    sed -i -r "s/^# \%wheel ALL=\(ALL\) ALL/\%wheel ALL=\(ALL\) ALL/" /etc/sudoers
    # X11 Keyboard configuration
    cp /etc/TUXconfig/SYSTEM/CONFIG/00-keyboard.conf /etc/X11/xorg.conf.d/00-keyboard.conf
    # Mouse Cursor configuration
    tar -xvzf /etc/TUXconfig/SYSTEM/CURSORS/bibata-modern-classic-cursors.tar.gz -C /usr/share/icons/
    echo -e "[Icon Theme]\nInherits=Bibata-Modern-Classic" > /usr/share/icons/default/index.theme
    # Fonts configuration
    mkdir -p /usr/local/share/fonts
    tar -xvzf /etc/TUXconfig/SYSTEM/FONTS/victor-mono-font.tar.gz -C /usr/local/share/fonts/
    fc-cache -vf
    # Enable the necessary services and configure systemd settings
    systemctl enable NetworkManager bluetooth lightdm cups systemd-timesyncd
    timedatectl set-ntp true
#  ##### }}}

#  #####  STEP 6  ##### {{{
# This step is to configure AUR packages and post-install customization/configuration
elif [ "$1" = "6" ]; then
    # Installation of Paru
    pushd /home/$(whoami)
    git clone https://aur.archlinux.org/paru.git
    cd paru
    makepkg -sirc
    cd ..
    rm -rf paru
    popd
    # Configure the appropriate location
    sudo timedatectl set-timezone America/Mexico_City
    sudo localectl set-x11-keymap latam
    # AUR installation
    paru -S awesome-git popcorntime-bin brave-bin samsung-unified-driver timeshift zsh-theme-powerlevel10k-git toilet code-minimap downgrade bpytop plymouth-git unimatrix-git pipes.sh tty-clock superproductivity tty-countdown-git zoom
    # Plymouth configuration
    sudo sed -i -r "s/^HOOKS.*/HOOKS=(base udev plymouth autodetect keyboard keymap modconf block plymouth-encrypt lvm2 filesystems fsck)/" /etc/mkinitcpio.conf
    sudo mkinitcpio -p linux-zen
    sudo sed -i -r "s@^GRUB_CMDLINE_LINUX_DEFAULT=.*@GRUB_CMDLINE_LINUX_DEFAULT=\"loglevel=3 quiet splash vt.global_cursor_default=0\"@" /etc/default/grub
    sudo grub-mkconfig -o /boot/grub/grub.cfg
    sudo systemctl disable lightdm
    sudo systemctl enable lightdm-plymouth
    # Set the plymouth theme
    sudo cp -r /etc/TUXconfig/PLYMOUTH/arch-hud /usr/share/plymouth/themes/arch-hud
    sudo plymouth-set-default-theme -R arch-hud
    # Update the plocate database
    sudo updatedb
#  ##### }}}

#  #####  STEP 7  ##### {{{
# This step is to mention further caveats and configurations not done during installation
elif [ "$1" = "7" ]; then
    echo -e "● Disable hardware acceleration from browsers (on each browser preferences) to stop videos from hanging and never loading."
    echo -e "● Check that all audio sources and inputs are unmuted (you can do this with pulsemixer or pavucontrol)."
    echo -e "● Check your network card with lspci and install the appropriate drives, otherwise internet will be slow, really slow... If using broadcom install the dkms one."
    echo -e "● If you want a different style for Brave New Tab, install the nightTab extension made by zombieFox on Chrome Store."
fi
#  ##### }}}
