#!/bin/sh

#       ████████  ███████   ██     ██ ██████
#      ██░░░░░░██░██░░░░██ ░██    ░██░█░░░░██
#     ██      ░░ ░██   ░██ ░██    ░██░█   ░██
#    ░██         ░███████  ░██    ░██░██████
#    ░██    █████░██░░░██  ░██    ░██░█░░░░ ██
#    ░░██  ░░░░██░██  ░░██ ░██    ░██░█    ░██
#     ░░████████ ░██   ░░██░░███████ ░███████
#      ░░░░░░░░  ░░     ░░  ░░░░░░░  ░░░░░░░
#      ████████ ████████ ██████████ ██     ██ ███████
#     ██░░░░░░ ░██░░░░░ ░░░░░██░░░ ░██    ░██░██░░░░██
#    ░██       ░██          ░██    ░██    ░██░██   ░██
#    ░█████████░███████     ░██    ░██    ░██░███████
#    ░░░░░░░░██░██░░░░      ░██    ░██    ░██░██░░░░
#           ░██░██          ░██    ░██    ░██░██
#     ████████ ░████████    ░██    ░░███████ ░██
#    ░░░░░░░░  ░░░░░░░░     ░░      ░░░░░░░  ░░
#
#    Author: Emilio Rivers (e-Rivers)

if [ $# -ne 0 ]; then
    if [ "$1" = "--set" ]; then
        if [ "$2" != "" ];then
            pushd /etc/TUXconfig/GRUB
            rm -rf /usr/share/grub/themes/*
            rm -rf /boot/grub/themes/*
            cp -rf "/etc/TUXconfig/GRUB/${2}" "/usr/share/grub/themes/${2}"
            cp -rf "/etc/TUXconfig/GRUB/${2}" "/boot/grub/themes/${2}"
            sed -i -r "s|[#]?\s*GRUB_THEME=\".*\"|GRUB_THEME=\"/boot/grub/themes/${2}/theme.txt\"|" /etc/default/grub 
            grub-mkconfig -o /boot/grub/grub.cfg
            popd
        else
            echo "No GRUB theme name was providen"
        fi
    elif [ "$1" = "--list" ]; then
        ls -d /etc/TUXconfig/GRUB/*/
    else
        echo "Unknown flag"
    fi
else
    echo -e "No flag was providen\n\n--list = Display available themes\n--set [THEME] = Sets the given theme"
fi
