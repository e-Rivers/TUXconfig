<div align="center">
  <h1>TUXconfig</h1>
  <img src="https://cdn.pling.com/img/b/e/6/4/45301f77a89acfb09a19884e3d661433860e.png" width="40%" height="30%">
</div>

___
### Description
Personal themes and dotfiles for my Linux OSes (display, grub, terminal, nvim, window manager, etc).<br/>
Installation and configuration scripts (not dotfiles) may vary from distro to distro because of package management and so on, so this ones are only proven to work with **Arch and Arch-Based systems**.

### Instructions
- First cd into the `/etc` folder since the setup and installation scripts make reference to it, plus, that's the folder for configuration files.
- Once there, clone this repo and if you are on vanilla Arch, after the final step (after installing GRUB) source the `setup.sh` script with the `--official-install` to install my predetermined software from the official repos. This is optional but some of the programs there are dependencies for the configuration scripts to work.
- Next run the `setup.sh` script again but with the `--system-config` to generate and edit the configuration files for things like setting the GRUB, the display manager, etc. 
- Now's time for user configuration, run`setup.sh` with `--user-config` flag specifying the username and groups (comma separated) to which the user will belong. Group selection recommended for the main user is **lp,sys,audio,video,wheel,input**.
- Reboot the machine and log into your new user, install paru following its [installation process](https://github.com/morganamilo/paru#installation). This can't be done before (in root user) since using makepkg (command necessary for manually installing AUR software) is not allowed with root privileges so it's necessary to log in with an normal account.
- After paru is installed, then again run `setup.sh` with `--aur-install` to install my predetermined software from the AUR repos.
- That's it for now.

### What is configured?
Configurations that are theme-independent
- GRUB theme
- Plymouth theme
- Mouse cursor theme
- Fonts
- Compositor

Configurations that are theme-dependent
- Display manager (`LigthDM webkit2`)
- Window manager (`AwesomeWM`)
- Terminal emulator (`Kitty`)
- System shell (`Zsh`)
- Application launcher (`Rofi`)
- Text editor (`Neovim`)
- System information (`Neofetch`)

### Packages & their description
<table>
  <tr>
    <td>Category</td>
    <td>Package(s)</td>
    <td>Purpose</td>
  </tr>
  <!-- FIRST CATEGORY (ENVIRONMENT) -->
  <tr>
    <td rowspan=4>Environment</td>
    <td>awesome</td>
    <td>Window Manager to be used</td>
  </tr>
  <tr>
    <td>xorg</td>
    <td>X server to be used</td>
  </tr>
  <tr>
    <td>xorg-init</td>
    <td>Xorg server utility to be able to use startx and other stuff</td>
  </tr>
  <tr>
    <td>xorg-xrandr</td>
    <td>Xorg server utility to be able manage multiple monitors</td>
  </tr>
  <!-- SECOND CATEGORY (DISPLAY MANAGER) -->
  <tr>
    <td rowspan=2>Display Manager and Lock Screen</td>
    <td>lightdm</td>
    <td>Display manager, which will also is used as lock screen with <i>dm-tool lock</i></td>
  </tr>
  <tr>
    <td>lightdm-webkit2-greeter</td>
    <td>lightdm backend to be used, although is slow, its highly customizable and that's why this one is used</td>
  </tr>
  <!-- THIRD CATEGORY (PRINTERS CONFIGURATION) -->
  <tr>
    <td rowspan=4>Printers Configuration</td>
    <td>cups</td>
    <td>Printers and printing managing service</td>
  </tr>
  <tr>
    <td>system-config-printer</td>
    <td>GUI interface for adding, editing and configuring printers</td>
  </tr>
  <tr>
    <td>gutenprint</td>
    <td>Drivers for a wide variety of printer brands</td>
  </tr>
  <tr>
    <td>samsung-unified-driver<sub>(AUR)</sub></td>
    <td>Drivers specific for Samsung printers</td>
  </tr>
  <!-- FOURTH CATEGORY (ZSH RELATED) -->
  <tr>
    <td rowspan=5>Zsh Related</td>
    <td>zsh</td>
    <td>Well, obviously, the Z-Shell</td>
  </tr>
  <tr>
    <td>zsh-autosuggestions</td>
    <td>Plugin to show autocompletion suggestions for partially written commands</td>
  </tr>
  <tr>
    <td>zsh-syntax-highlighting</td>
    <td>Plugin to highlight when a command is correct or not</td>
  </tr>
  <tr>
    <td>zsh-history-substring-search</td>
    <td>Plugin to be able to search trough zsh history based on a partially written command</td>
  </tr>
  <tr>
    <td>zsh-theme-powerlevel10k-git<sub>(AUR)</sub></td>
    <td>The famous and great looking powerlevel prompt for Z shell</td>
  </tr>
</table>
