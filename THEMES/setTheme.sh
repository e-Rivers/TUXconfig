#!/bin/sh

if [ $# -ne 0 ]; then
    if [ "$1" = "--set" ]; then
        if [ "$2" != "" ];then
            pushd "/etc/TUXconfig/THEMES/${2}"
            rm -rf /usr/share/lightdm-webkit/themes/*
            cp -rf "/etc/TUXconfig/THEMES/${2}/${2}" "/usr/share/lightdm-webkit/themes/${2}"
            sed -i -r "s/^webkit_theme.*/webkit_theme        = ${2}/" /etc/lightdm/lightdm-webkit2-greeter.conf
            # systemctl restart lightdm
            popd
        else
            echo "No LIGHTDM theme name was providen"
        fi
    elif [ "$1" = "--list" ]; then
        ls -d /etc/TUXconfig/LIGHTDM/*/ #### THIS LINE IS WRONG
    else
        echo "Unknown flag"
    fi
else
    echo -e "No flag was providen\n\n--list = Display available themes\n--set [THEME] = Sets the given theme"
fi

